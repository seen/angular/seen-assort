/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('Assort service module: $assort', function() {
	var $assort;
	var SeenPaginatorParameter;
	var $rootScope;
	var $timeout;
	var $httpBackend;

	beforeEach(function() {
		module('seen-assort');
		inject(function(_$assort_, _$rootScope_, _$httpBackend_, _$timeout_,
				_SeenPaginatorParameter_) {
			$assort = _$assort_;
			SeenPaginatorParameter = _SeenPaginatorParameter_;
			$rootScope = _$rootScope_;
			$httpBackend = _$httpBackend_;
			$timeout = _$timeout_;
		});
	});

	// check to see if it has the expected function
	it('should be defined', function() {
		expect(angular.isDefined($assort)).toBe(true);
	});
	

	it('should call /api/assort/category/new to create new category', function(done) {
		var id = 1;
		var data = {
			id : id,
			title : 'content title',
			description : 'content description'
		};
		$assort.newCategory(data)//
		.then(function(object) {
			expect(object.hasOwnProperty('id')).toBe(true);
			expect(object).not.toBeNull();
			expect(object.id).not.toBeUndefined();
			expect(object.id).toBe(id);
			expect(object.title).toBe(data.title);
			expect(object.description).toBe(data.description);
			done();
		});

		$httpBackend//
			.expect('POST', '/api/assort/category/new')//
			.respond(200, data);
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});


	it('should call /api/assort/tag/new to create new tag', function(done) {
		var id = 1;
		var data = {
			id : id,
			title : 'content title',
			description : 'content description'
		};
		$assort.newTag(data)//
		.then(function(object) {
			expect(object.hasOwnProperty('id')).toBe(true);
			expect(object).not.toBeNull();
			expect(object.id).not.toBeUndefined();
			expect(object.id).toBe(id);
			expect(object.title).toBe(data.title);
			expect(object.description).toBe(data.description);
			done();
		});

		$httpBackend//
			.expect('POST', '/api/assort/tag/new')//
			.respond(200, data);
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});

});
