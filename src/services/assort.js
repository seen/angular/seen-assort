/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('seen-assort')

/**
 * @memberof seen-shop
 * @ngdoc service
 * @name $shop
 * @description 
 * 
 */
.service('$assort', function($backend, SeenObjectFactory, AssortCategory, AssortTag) {

	var _categoryCache = new SeenObjectFactory(function(data) {
		return new AssortCategory(data);
	});

	/**
	 * List all categories.
	 */
	this.categories = $backend.createFind({
		url: '/api/assort/category/find'
	}, _categoryCache);

	/**
	 * Gets a gategory
	 */
	this.category = $backend.createGet({
		url: '/api/assort/category/{id}'
	}, _categoryCache);

	/**
	 * Create new instance of category
	 */
	this.newCategory = $backend.createNew({
		url: '/api/assort/category/new'
	}, _categoryCache);


	/*
	 * Tag factory
	 */
	var _tagCache = new SeenObjectFactory(function(data) {
		return new AssortTag(data);
	});

	/**
	 * List all tags.
	 */
	this.tags = $backend.createFind({
		url: '/api/assort/tag/find'
	}, _tagCache);
	
	/**
	 * Gets tag
	 */
	this.tag = $backend.createGet({
		url: '/api/assort/tag/{id}'
	}, _tagCache);
	
	/**
	 * Create new instance of tag
	 */
	this.newTag = $backend.createNew({
		url: '/api/assort/tag/new'
	}, _tagCache);

});