# Seen Assort

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-assort/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-assort/commits/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/be37ae65702a4eb9a539d7d9cbdfca79)](https://www.codacy.com/app/seen/seen-assort?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-assort&amp;utm_campaign=Badge_Grade)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-assort/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-assort/commits/develop)